/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 19, 2014, 4:21 PM
 */

#include <cstdlib>
#include <iostream> // This is the iostream library.
// It allows input and output

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    cout << "Michael Shaw" << endl << endl; // Semicolon
    return 0;
}

